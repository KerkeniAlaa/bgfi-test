package com.bgfi.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author Alaa KERKENI
 *
 */
public class ListUtils {

	/**
	 * Retourne une liste de sous listes liste, où chaque sous liste a au
	 * maximum <b>taille</b> éléments.
	 * 
	 * @param list
	 *            liste à partitionner
	 * @param taille
	 *            taille maximale de sous liste
	 * @return une liste de sous listes liste, où chaque sous liste a au maximum
	 *            <b>taille</b> éléments
	 * @throws IllegalArgumentException
	 *             lorsqu'on met une taille négative
	 */
	public static <E> List<List<E>> partition(List<E> list, int taille)
			throws IllegalArgumentException {
		// on vérifie tout d'abord la taille si elle est bien > 0
		if (taille > 0) {
			// on détermine le nombre de sous listes (partie entière supérieure)
			int nbrSousListe = (int) Math.ceil((double) list.size() / taille);
			List<List<E>> lists = new ArrayList<List<E>>();

			Iterator<E> it = list.iterator();
			for (int i = 0; i < nbrSousListe; i++) {
				List<E> sousListe = new ArrayList<E>();
				while ((sousListe.size() < taille) && (it.hasNext())) {
					sousListe.add(it.next());
				}
				lists.add(sousListe);
			}
			return lists;
		} else {
			throw new IllegalArgumentException("taille de partitionnement illégale");
		}
	}
}
