package com.bgfi.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.Assert;


/**
 * Test de la classe ListUtils
 * @author Alaa KERKENI
 */
public class ListUtilsTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	/**
	 * Test de l'exception et du message d'exception de la méthode partition
	 */
	@Test
	public void partitionExceptionTest() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("taille de partitionnement illégale");
		ListUtils.partition(Arrays.asList(1, 2, 3), 0);
	}

	/**
	 * Test de la méthode partition avec une liste d'entiers et une taille de
	 * sous liste = 3
	 */
	@Test
	public void partitionIntTest() {
		List<Integer> intList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
		List<List<Integer>> expectedList = new ArrayList<List<Integer>>();
		expectedList.add(Arrays.asList(1, 2, 3));
		expectedList.add(Arrays.asList(4, 5, 6));
		expectedList.add(Arrays.asList(7, 8));
		Assert.assertEquals(expectedList, ListUtils.partition(intList, 3));
	}

	/**
	 * Test de la méthode partition avec une liste de String et une taille de
	 * sous liste = 5
	 */
	@Test
	public void partitionStringTest() {
		List<String> stringList = Arrays.asList("aa", "bb", "cc", "dd", "ee", "ff", "gg",
				"hh");
		List<List<String>> expectedList = new ArrayList<List<String>>();
		expectedList.add(Arrays.asList("aa", "bb", "cc", "dd", "ee"));
		expectedList.add(Arrays.asList("ff", "gg", "hh"));
		Assert.assertEquals(expectedList, ListUtils.partition(stringList, 5));
	}
	
	/**
	 * Test de la méthode partition avec une liste de String et une taille de
	 * sous liste supérieure à la taille de la liste (exemple taille = 24)
	 */
	@Test
	public void partitionTailleTest() {
		List<String> stringList = Arrays.asList("aa", "bb", "cc", "dd", "ee", "ff", "gg",
				"hh");
		List<List<String>> expectedList = new ArrayList<List<String>>();
		expectedList.add(stringList);
		Assert.assertEquals(expectedList, ListUtils.partition(stringList, 24));
	}

}
